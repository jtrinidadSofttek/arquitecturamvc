﻿using Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UI_MVCNegocios.Models
{
    public class EmpleadoViewModel
    {
        //Propiedades
        public int Id { get; set; }
        [Required]
        [MinLength(2, ErrorMessage = "El nombre ingresado es demasiado corto.")]
        [MaxLength(40, ErrorMessage = "El nombre ingresado es demasiado largo.")]
        public string Nombre { get; set; }
        [Range(0, 99999999)]
        [Required]
        public int Dni { get; set; }
        public int Id_Negocio { get; set; }
        [DisplayName("Negocio")]
        public string nombreNegocio { get; set; }
        [Range(0, 999999999)]
        public double Sueldo { get; set; }
        public virtual Negocio Negocio { get; set; }

        //Constructor

        public EmpleadoViewModel(Empleado empleado)
        {
            Id = empleado.Id;
            Nombre = empleado.Nombre;
            Dni = empleado.Dni;
            Sueldo = empleado.Sueldo;
            Negocio = empleado.Negocio;
            Id_Negocio = empleado.Id_Negocio;
            nombreNegocio = new NegocioBusiness.ABMNegocio().GetNegocio(empleado.Id_Negocio).Nombre;
        }

        public EmpleadoViewModel() { }

        public Empleado aEmpleado()
        {
            Empleado e = new Empleado();
            e.Id = Id;
            e.Nombre = Nombre;
            e.Dni = Dni;
            e.Sueldo = Sueldo;
            e.Id_Negocio = Id_Negocio;
            e.Negocio = Negocio;
            return e;
        }
    }
}