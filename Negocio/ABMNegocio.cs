﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using AccesoADatos;
using Models;

namespace NegocioBusiness
{
    public class ABMNegocio
    {
        private Repository rp { get; }

        public ABMNegocio() => rp = new Repository();
        
        public List<Negocio> GetNegocios() => rp.GetNegocios();

        public Negocio GetNegocio(int id) => rp.GetNegocio(id);

        public void AddNegocio(Negocio n) => rp.AddNegocio(n);

        public void RemoveNegocio(Negocio n) => rp.RemoveNegocio(n);

        public void UpdateNegocio(Negocio n) => rp.UpdateNegocio(n);

        public List<Empleado> FilterNegocio(int id) => new ABMEmpleado().GetEmpleados().Where(x => x.Id_Negocio == id).ToList();
    }
}
