﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace AccesoADatos
{
    using SqlProviderServices = System.Data.Entity.SqlServer.SqlProviderServices;

    public class Repository
    {
        private CarniceriaEntities db { get; }

        public Repository() => db = new CarniceriaEntities();

        public List<Negocio> GetNegocios() => db.Negocio.ToList();
        
        public Negocio GetNegocio(int id) => db.Negocio.FirstOrDefault(x => x.Id == id);

        public void AddNegocio(Negocio n)
        {
            db.Negocio.Add(n);
            db.SaveChanges();
        }

        public void RemoveNegocio(Negocio n)
        {
            db.Negocio.Remove(n);
            db.SaveChanges();
        }

        public void UpdateNegocio(Negocio n)
        {
            Negocio nw = GetNegocio(n.Id);
            nw.Direccion = n.Direccion;
            nw.Email = n.Email;
            nw.Nombre = n.Nombre;
            db.SaveChanges();
        }

        public List<Empleado> GetEmpleados() => db.Empleado.ToList();
        public List<Empleado> GetEmpleados(int id) => db.Empleado.Where(x=> x.Id_Negocio ==id).ToList();
        public Empleado GetEmpleado(int id) => db.Empleado.FirstOrDefault(x => x.Id == id);

        public void AddEmpleado(Negocio n, Empleado e)
        {
            _ = e.Id_Negocio = n.Id;
            db.Empleado.Add(e);
            db.SaveChanges();
        }

        public void RemoveEmpleado(Empleado e)
        {
            db.Empleado.Remove(e);
            db.SaveChanges();
        }

        public void UpdateEmpleado(Empleado e)
        {
            Empleado em = GetEmpleado(e.Id);
            em.Nombre = e.Nombre;
            em.Dni = e.Dni;
            em.Id_Negocio = e.Id_Negocio;
            em.Sueldo = e.Sueldo;
            db.SaveChanges();
        }

    }
}
